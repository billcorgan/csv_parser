# -*- coding: utf-8 -*-
"""
Created on Mon Apr  5 09:17:35 2021

@author: user
"""
import xml.etree.ElementTree as ET

fr_codes_to_pp = {
                    1757:"FR17637",
                    1623:"FR17636",
                    1734:"FR17635",
                    1736:"FR17634",
                    522:"FR17633",
                    470:"FR17632",
                    525:"FR17631",
                    546:"FR17630",
                    451:"FR17629",
                    538:"FR17628",
                    32:"FR17627",
                    33:"FR17626",
                    528:"FR17625",
                    475:"FR17624",
                    31:"FR17623",
                    23:"FR17622",
                    21:"FR17621",
                    20:"FR17620",
                    17:"FR17619",
                    22:"FR17618",
                    24:"FR17617",
                    18:"FR17616",
                    19:"FR17615",
                    13:"FR17614",
                    16:"FR17613",
                    4:"FR17612",
                    15:"FR17611",
                    1834:"FR17610",
                    39:"FR17609",
                    38:"FR17608",
                    37:"FR17607",
                    36:"FR17606",
                    35:"FR17605",
                    34:"FR17604",
                    39:"FR17603",
                    46:"FR17602",
                    548:"FR17601",
                    4948:"FR17600",
                    1:"FR17599",
                    481:"FR17598",
                    478:"FR17597",
                    479:"FR17596",
                    44:"FR17595",
                    12:"FR17594",
                    40:"FR17593",
                    47:"FR17592",
                    178:"FR17591",
                    14:"FR17590",
                    9:"FR17589"
}

# invert dict
fr_codes_to_pp_invert = {v: k for k, v in fr_codes_to_pp.items()}

if __name__ == '__main__':
    
    outf = open('output.csv', "a")

    root = ET.parse('input.xml').getroot()

    message = ET.fromstring(root[0][0][0][0][1].text)
    for child1 in message:
        for child2 in child1:
            if child2.text in fr_codes_to_pp_invert:
                outf.write("," + str(fr_codes_to_pp_invert[child2.text]) + ",")
            else:
                outf.write(child2.text)
        outf.write("\n")
        
    outf.close()


